/*====================================IMPORT MODULES====================================*/
import { burgerOpener, modalOpener } from './modules/libs';
/*======================================================================================*/


/*======================================DOM LOADED======================================*/
window.addEventListener('DOMContentLoaded', () => {
    burgerOpener('burger', 'close', '.overlay-menu', '.menu__item-link');
    modalOpener();


    // Registration --------------------------------------//
    window.onload = () => {
        AOS.init();
        const wheel = document.querySelector('.wheel-overlay');
        // const giftOverlay = document.querySelector('.overlay__gift');
        const overlay_reg = document.querySelector('#overlay-reg');
        const giftBtn = document.querySelector('.gift__close');
        const wheel_btn = document.querySelector('.bigButton');
    
        if(giftBtn !== null) {
            giftBtn.addEventListener('click', () => {
                giftOverlay.classList.remove('active');
                wheel.classList.remove('active');
                wheel_btn.disabled = false;
                setTimeout(() => {
                    location.reload();
                }, 1000)
            })
        }
    
        window.responseText = 'none';
    
        if(document.getElementsByTagName('form')[0] != undefined) {
            $(".phone").mask("380999999999", { placeholder: "380_________" });
    
            function sendForm(index) {
                //Send form
                let
                formBlock = document.getElementsByTagName("form")[index];
            
                if(formBlock != undefined) {
                    let formBlock_CodeInput = formBlock.querySelector("input[name='code']");
                    formBlock.addEventListener("submit", async(event) => {
            
                        event.preventDefault();
                    
                        //Values
                        let
                            xhr = new XMLHttpRequest(),
                            data = new FormData(event.target),
                            url = formBlock.getAttribute("action"),
                            method = formBlock.getAttribute('method'),
                            result_Block = formBlock.querySelector(".result-text"),
                            submit_button = formBlock.querySelector("input[type='submit'], button[type='submit']"),
                            loader = formBlock.getElementsByClassName("loader")[0];
                    
                        //Disable submit-button
                        submit_button.disabled = true;
                        //Show loader
                        loader.classList.add('active');
                     
                        grecaptcha.ready(function() {
                            grecaptcha.execute('6Ld13OcaAAAAAG8rKkfe0ykgiS7fp4Z1wjE8uYL1', {action: 'submit'}).then(function(token) {
                    
                                //Add token
                                data.append('g-recaptcha-response', token);
    
                                xhr.open(method, url);
                                xhr.responseType = 'text';
                                xhr.setRequestHeader("Cache-Control", "no-cache");
                                xhr.setRequestHeader("redirect", "follow");
                                xhr.setRequestHeader("referrerPolicy", "no-referrer");
                                xhr.setRequestHeader("mode", "cors");
                                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    
                                xhr.onload = function() {
                    
                                    submit_button.disabled = false;
                                    loader.classList.remove('active');
                    
                                    if (this.status >= 200 && this.status < 300) {

                                        result_Block.style.color = "#44944A";

                                        // const image = document.querySelector('.coupon__image');
                                        // const srcValue = document.querySelector('#code__input');
                                        // image.src = `/api/gift/${srcValue.value}`;

                                        // console.log(xhr.response, '1');

                                        // const btn = document.querySelector('.show-coupon');
                                        // btn.style.display = 'block';
                                        
    
                                        if(index === 1) {
                                            
                                            result_Block.style.display = 'none';
                                            window.location.href = '/account.html';
                                        }
                                        else {
                                            responseText = xhr.response;
                                            result_Block.textContent = 'зберігайте чек з кодом';
                                            // console.log(xhr.response, '2');
                                        }
                                        
                                    } else {
                                        result_Block.style.color = "#F13A13";
                                        //Clear input
                                    }
                                    // console.log(xhr.response, '3');
                                    if(index !== 1)
                                        result_Block.textContent = (this.status >= 500) ? "Час очікування відповіді сервера минув" : xhr.response;
                                }
                    
                                xhr.send(data);               
                              
                            });
                        });

                    });
                }
            }
    
            sendForm(0);
            sendForm(1);
        }
    
    }
    

    // Get User Info
    if(window.location.href.includes('account')) {

        let user_phone = readCookie('phone');

        function readCookie(name) {
            let matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }

        let xhr = new XMLHttpRequest(),
            data = new FormData();

        // data.append('user_id', user_id);

        xhr.open('GET', `/api/getInfo`);
        xhr.responseType = 'json';
        xhr.setRequestHeader("Cache-Control", "no-cache");
        xhr.setRequestHeader("redirect", "follow");
        xhr.setRequestHeader("referrerPolicy", "no-referrer");
        xhr.setRequestHeader("mode", "cors");
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

        xhr.onload = function() {

            if (this.status >= 200 && this.status < 300) {

                // console.log(xhr.response, '4');
                let responseArray = xhr.response;
                let objectArray = ['date', 'code', 'win'];
                

                function addElementsToDocument() {
                    let tablet = document.querySelector('.tablet');
                    let phone = document.querySelector('.account_phone');
        
                    function addLi(index) {
                        let li = document.createElement('li');
                        li.setAttribute('class', 'tablet__item');
                        
                        for(let i = 0; i < 2; i++) {
                            addP(li, responseArray[index][objectArray[i]]);
                        }
                        for(let i = 0; i < 1; i++) {
                            addButton(li, responseArray[index][objectArray[2]]);
                        }
                        tablet.insertAdjacentElement('beforeend', li);
                    }
            
                    function addP(li, elem) {
                        let p = document.createElement('p');

                        p.innerText = elem;
                        li.insertAdjacentElement('beforeend', p);
                    }

                    function addButton(li, elem) {
                        let button = document.createElement('button');
                        button.setAttribute('class', 'show-image');

                        button.innerText = 'Подарунок';
                        li.insertAdjacentElement('beforeend', button);
                    }

                    phone.innerText = user_phone;

                    for(let i = 0; i < responseArray.length; i++) {
                        addLi(i);
                    }



                    // --------------------------------------------
                    function bindModal(overlaySelector, formID, openBtnSelector, closeBtnSelector, inputWrapperSelector = null, errorLabel = null) {

                        const overlay = document.querySelector(overlaySelector),
                            modal = document.getElementById(formID),
                            openBtn = openBtnSelector,
                            closeBtn = document.querySelectorAll(closeBtnSelector);
                
                        let removeScroll = 0;
                        let isWrapper = false;
                
                        if(inputWrapperSelector !== null) {
                            const wrappers = document.querySelectorAll(inputWrapperSelector),
                                labels = document.querySelectorAll(errorLabel);
                            isWrapper = true;
                        }
                
                        if(modal !== null) {
                
                                let user_id = readCookie('user_id');
                
                                function readCookie(name) {
                                    let matches = document.cookie.match(new RegExp(
                                        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                                    ));
                                    return matches ? decodeURIComponent(matches[1]) : undefined;
                                }
                
                                if(user_id && modal.id === 'modal-auth') {
                                    openBtn.addEventListener('click', (e) => {
                                        e.preventDefault();
                                        window.location.href = '/account.html';
                                    })
                                } else {
                                    
                                        openBtn.addEventListener('click', (e) => {
                                            e.preventDefault();
                                            overlay.classList.add('active');
                                            modal.classList.add('active');
                                            document.body.style.overflowY = 'hidden';
                                            document.body.style.marginRight = `0px`; 
                                        });
                
                                        closeBtn.forEach(btn => {
                
                                            btn.addEventListener('click', () => {
                                                
                                                overlay.classList.remove('active');
                                                modal.classList.remove('active');
                                                document.body.style.overflowY = 'scroll';
                                                document.body.style.marginRight = `0px`; 

                                            });
                                        });
                
                                        overlay.addEventListener('click', (e) => {
                
                                            if(e.target === overlay) {
                                
                                                overlay.classList.remove('active');
                                                modal.classList.remove('active');
                                                document.body.style.overflowY = 'scroll';
                                                document.body.style.marginRight = `0px`; 
                
                                            }
                                        });
                                }
                
                        };
                
                    };


                    // Buttons ----------------------------------------------------
                    const btns = document.querySelectorAll('.show-image');
                    if(btns) {
                        for(let i = 0; i < btns.length; i++) {

                            btns[i].addEventListener('click',() => {
                                const image = document.querySelector('.coupon__image');
                                image.src = `/api/gift/${responseArray[i].code}`;
                
                            })

                            bindModal('#overlay-img', 'modal-img', btns[i], '.close-auth-modal');
                        }
                    }
                } 
                
                addElementsToDocument();

            }
        }

        xhr.send(data);         
    }

    const logout = document.querySelector('.logout');
    if(logout !== null) {
        logout.addEventListener('click', () => {

            function createCookie(name,value,days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime()+(days*24*60*60*1000));
                    var expires = "; expires="+date.toGMTString();
                }
                else var expires = "";
                document.cookie = name+"="+value+expires+"; path=/";
            }

            function eraseCookie(name) {
                createCookie(name,"",-1);
            }
              
                eraseCookie('user_id');
              window.location ='/';
        })
    }



});
/*======================================================================================*/
